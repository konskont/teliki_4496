package org.example;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class MessageServiceTest {


    Network network = Mockito.mock(Network.class);

    @Test
    public void messageDeliveryWasSuccessful(){
        Mockito.when(network.sendMessage("192.168.1.1","Message was received")).thenReturn(true);

        MessageService messageService = new MessageService();
        messageService.sendMessage("192.168.1.1","Message was received");
        assertTrue(messageService.sendMessage("192.168.1.1","Message was received"));
    }

    @Test
    public void messageDeliveryWasFailed(){
        Mockito.when(network.sendMessage("192.168.1.1","Message was not received")).thenReturn(false);
        MessageService messageService = new MessageService();
        messageService.sendMessage("192.168.1.1","Message was not received");
        assertTrue(messageService.sendMessage("192.168.1.1","Message was not received"));
    }

}
